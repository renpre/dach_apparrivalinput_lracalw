/**
	Appointment Arrival Input Widget
	2013-12-12 niczij
**/
var controller = (function ($) {
	var wwClient = qmatic.webwidget.client,
		connectorClient = qmatic.connector.client;
		
		function inputEvents(topic, data) {
			$("#inputAppArrivalCode").val(JSON.parse(data).data);
			$("#inputAppArrivalCode").attr("placeholder", JSON.parse(data).placeholder);
			if (JSON.parse(data).show) {
				$("#inputAppArrivalCode").fadeIn("normal", function() {
					$("#inputAppArrivalCode").css({width: $(document).width(), height: $(document).height()});
				});
			} else {
				$("#inputAppArrivalCode").fadeOut();
			}
		}	
	return {
		onLoaded : function (configuration) {
			var attr = configuration.attributes,
				attribParser = new qmatic.webwidget.AttributeParser(attr || {});
			//get the widget attributes
			var font = attribParser.getString("input.font", "Arial;22px;normal;normal").split(";"),
				fontColor = attribParser.getString("input.font.color", "#555555"),
				backgroundColor = attribParser.getString("input.background.color", "#ffffff"),
				border = attribParser.getString("input.border", "1px solid #cccccc"),
				borderRadius = attribParser.getString("input.border.radius", "4px 4px 4px 4px");
			$("#inputAppArrivalCode").css({fontFamily: font[0], fontSize: font[1], fontStyle: font[2], fontWeight: font[3]});
			$("#inputAppArrivalCode").css({color: fontColor, backgroundColor: backgroundColor, border: border, borderRadius: borderRadius});
			wwClient.subscribe("appArrival.widget.input", inputEvents);
		},
		onLoadError : function (message) {
			$('body').append('<p>Widget load error: ' + message + '</p>');
		}
	};
})(jQuery);